
import pygame

# Game title
windowtitle = "Simple Pygame Game"

# Game size
windowwidth = 1000
windowheight = 1000

# Some random colours
black = (0,0,0)
white = (255,255,255)

# Game refresh rate
clock = pygame.time.Clock()

# Font
pygame.font.init()
font = pygame.font.SysFont("Arial", 100)

# The entire game lmfao
class FirstGame:

    fps = 144

    def __init__(self, imagepath, title, width, height):
        self.title = title
        self.width = width
        self.height = height

        # Background render
        self.window = pygame.display.set_mode((width, height))
        self.window.fill(white)
        pygame.display.set_caption(title)
        backgroundimage = pygame.image.load(imagepath)
        self.image = pygame.transform.scale(backgroundimage, (width, height))

    def thisisthegame(self, stagespeed):

        # Is player dead
        ded = False
        victory = False

        direction = 0

        # Treasure
        goal = GameEntity("treasure.png", 475, 50, 50, 50)

        # player
        human = Player("player.png", 475, 875, 50, 50)
        

        # Enemy and Stage increase

        roomba0 = NPC("enemy.png", 20 , 700, 50, 50)
        roomba1 = NPC("enemy.png", self.width - 40 , 500, 50, 50)
        roomba2 = NPC("enemy.png", self.width - 60 , 300, 50, 50)

        roomba0.speed *= stagespeed
        roomba1.speed *= stagespeed
        roomba2.speed *= stagespeed
        
        while not ded:

            for event in pygame.event.get():
                
                # Recording key presses
                if event.type == pygame.QUIT:
                    ded = True
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        direction = 1
                    elif event.key == pygame.K_DOWN:
                        direction = -1
                elif event.type == pygame.KEYUP:
                    if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                        direction = 0

                print(event)
            
            # Background refresh
            self.window.fill(white)
            self.window.blit(self.image, (0,0))

            # Goal entity
            goal.draw(self.window)

            # Sammi Entiies
            roomba0.move(self.width)
            roomba0.draw(self.window)

            if stagespeed > 2:
                roomba1.move(self.width)
                roomba1.draw(self.window)
            if stagespeed > 4:
                roomba2.move(self.width)
                roomba2.draw(self.window)

            # Player
            human.move(direction, self.height)
            human.draw(self.window)

            # Win/Lose Conditions
            if human.collision(roomba0) or human.collision(roomba1) or human.collision(roomba2):
                ded = True
                victory = False
                text = font.render("You suck lol", True, black)
                self.window.blit(text, (250, 450))
                pygame.display.update()
                clock.tick(1)
                break

            elif human.collision(goal):
                ded = True
                victory = True
                text = font.render("Nice", True, black)
                self.window.blit(text, (410, 450))
                pygame.display.update()
                clock.tick(1)
                break
            
            # FPS
            pygame.display.update()
            clock.tick(self.fps)
        
        # Recursion
        if victory:
            self.thisisthegame(stagespeed + 0.5)
        else:
            return

class GameEntity:

    def __init__(self, imagepath, x, y, width, height):
        self.width = width
        self.height = height
        self.xpos = x
        self.ypos = y

        entity = pygame.image.load(imagepath)
        self.entity = pygame.transform.scale(entity, (width, height))
    
    def draw(self, background):
        background.blit(self.entity, (self.xpos, self.ypos))


class Player(GameEntity):

    speed = 5

    def __init__(self, imagepath, x, y, width, height):
        super().__init__(imagepath, x, y, width, height)
    
    # Player movement behaviour
    def move(self, direction, max_height):
        if direction > 0:
            self.ypos -= self.speed
        elif direction < 0:
            self.ypos += self.speed
        # Padding
        if self.ypos >= max_height - 90:
            self.ypos = max_height - 90   

    # Collision Detection
    def collision(self, entity):
        if self.ypos > entity.ypos + entity.height:
            return False
        elif self.ypos + self.height < entity.ypos:
            return False
        if self.xpos > entity.xpos + entity.width:
            return False
        elif self.xpos + self.height < entity.xpos:
            return False

        return True


class NPC(GameEntity):
    
    speed = 3
    
    def __init__(self, imagepath, x, y, width, height):
        super().__init__(imagepath, x, y, width, height)
    
    # NPC movement behaviour
    def move(self, max_width):
        if self.xpos <= 20:
            self.speed = abs(self.speed)
        elif self.xpos >= max_width - 90:
            self.speed = -abs(self.speed)
        
        self.xpos += self.speed

pygame.init() # Initializes game

newgame = FirstGame("background.png", windowtitle, windowwidth, windowheight)
newgame.thisisthegame(1)

# Lol you quit
pygame.quit()
quit()


    
# Dont need this but good to know
    # pygame.draw.rect(game, black, [450,450,100,100])
    # pygame.draw.circle(game, black, (500,400), 50))

    # game.blit(player, (475,475))
